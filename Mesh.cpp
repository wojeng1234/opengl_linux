#include "Mesh.hpp"

Mesh::Mesh(const void* data, size_t size)
: m_size(size)
{
    glGenVertexArrays(1, &m_vao);
    glGenBuffers(1, &m_buffer);
    glBindVertexArray(m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(m_vao);
}

Mesh::~Mesh()
{
    glDeleteBuffers(1, &m_buffer);
    glDeleteVertexArrays(1, &m_vao);
}

void Mesh::Bind()
{
    glBindVertexArray(m_vao);
}