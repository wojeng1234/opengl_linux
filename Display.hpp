#ifndef DISPLAY_HPP
#define DISPLAY_HPP

#include <string>
#include <SDL2/SDL.h>
#include <GL/glew.h>

class Display
{
public:
    Display(const std::string& name, int width, int height);
    ~Display();

    void Update();
    void Clear();
    inline bool isClosed() { return m_isClosed; }

private:
    bool m_isClosed;
    SDL_Window* m_Window;
    SDL_GLContext m_Context;
};

#endif // DISPLAY_HPP