#include "Shader.hpp"

static int LoadShader(
    const std::string& filePath,
    std::string& shaderCode
    );

static int CheckShaderError(
    GLuint program,
    GLuint status,
    bool isProgram,
    const std::string& errorMessage
    );

static int CreateShader(
    GLuint* shader,
    const std::string& shaderCode,
    GLenum type
    );

Shader::Shader(const std::string& shaderName)
{
    m_Program = glCreateProgram();
    GLuint vertex_shader = 0;
    GLuint fragment_shader = 0;

    std::string vertex_shader_code;
    LoadShader("../res/shaders/basic_v.glsl", vertex_shader_code);
    std::string fragment_shader_code;
    LoadShader("../res/shaders/basic_f.glsl", fragment_shader_code);

    CreateShader(
        &vertex_shader,
        vertex_shader_code,
        GL_VERTEX_SHADER);

    CreateShader(
        &fragment_shader,
        fragment_shader_code,
        GL_FRAGMENT_SHADER);

    glAttachShader(m_Program, vertex_shader);
    glAttachShader(m_Program, fragment_shader);

    glLinkProgram(m_Program);
    CheckShaderError(
        m_Program, GL_LINK_STATUS, true,
        shaderName + " linking error: "
    );

    glValidateProgram(m_Program);
    CheckShaderError(
        m_Program, GL_VALIDATE_STATUS, true,
        shaderName + " linking error: "
    );

    //Should I do this?
    glDetachShader(m_Program, vertex_shader);
    glDetachShader(m_Program, fragment_shader);
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
}

void Shader::Bind()
{
    glUseProgram(m_Program);
}

Shader::~Shader()
{
    glDeleteProgram(m_Program);
}

static int CreateShader(
    GLuint* shader,
    const std::string& shaderCode,
    GLenum type
    )
{
    *shader = glCreateShader(type);

    if(shader == 0)
    {
        std::cout << "Error in " << __FUNCTION__
            << " glCreateShader failed" << std::endl;
        return -1;
    }

    const char* shaderCodePtr[1];
    shaderCodePtr[0] = shaderCode.c_str();
    GLint shaderCodeLength[1];
    shaderCodeLength[0] = shaderCode.size();
    glShaderSource(
        *shader, 1, shaderCodePtr,
        shaderCodeLength
        );
    
    glCompileShader(*shader);
    return CheckShaderError(
        *shader, GL_COMPILE_STATUS, false,
        "Error: Compile shader error: "
        );
}

static int LoadShader(
    const std::string& filePath,
    std::string& shaderCode
    )
{
    std::ifstream inFile;
    std::string line; 

    inFile.open(filePath);
    if(inFile.good())
    {
        while(std::getline(inFile, line))
        {
            shaderCode += line;
            shaderCode += "\n";
        }
    }
    else
    {
        std::cout << "From: " << __FUNCTION__ << " shader file load failed\n";
        return -1;
    }

    return 0;
}

static int CheckShaderError(
    GLuint program,
    GLuint flag,
    bool isProgram,
    const std::string& errorMessage
    )
{
    #define ERR_SIZE 1024
    GLint isSuccess = 0;
    GLchar error[ERR_SIZE] = {0};

    if(isProgram)
        glGetProgramiv(program, flag, &isSuccess);
    else
        glGetShaderiv(program, flag, &isSuccess);

    if(isSuccess == GL_FALSE)
    {
        if(isProgram)
            glGetProgramInfoLog(program, ERR_SIZE, NULL, error);
        else
            glGetShaderInfoLog(program, ERR_SIZE, NULL, error);

        std::cout << errorMessage << error << std::endl;
        return -1;
    }

    return 0;
}