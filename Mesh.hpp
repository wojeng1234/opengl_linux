#ifndef MESH_HPP
#define MESH_HPP

#include <GL/glew.h>
#include <glm/glm.hpp>

class Mesh
{
public:
    Mesh(const void* data, size_t size);
    ~Mesh();
    void Bind();
    inline size_t getSize() { return m_size; }

private:
    GLuint m_vao;
    GLuint m_buffer;
    size_t m_size;
};

#endif // MESH_HPP