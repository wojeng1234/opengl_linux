#include <stdio.h>
#include <iostream>
#include <AppConfig.h>
#include "Display.hpp"
#include "Shader.hpp"
#include "Mesh.hpp"

int main(int argc, char* argv[])
{
    glm::vec3 positions[3] = {
        glm::vec3(-0.5f, -0.5f, 0.0f),
        glm::vec3( 0.0f,  0.5f, 0.0f),
        glm::vec3( 0.5f, -0.5f, 0.0f)
    };

    printf("Version: %d.%d.%d\n", App_VERSION_MAJOR, App_VERSION_MINOR, App_VERSION_PATCH);
    Display display("Hello cruel world!", 800, 600);
    
    Shader sh("heheszki");
    Mesh me(positions, 3 * sizeof(glm::vec3));
    Mesh me2(pos2, 3 * sizeof(glm::vec3));

    while(!display.isClosed())
    {
        display.Clear();
        sh.Bind();
        me.Bind();
        glDrawArrays(GL_TRIANGLES, 0, me.getSize()/sizeof(glm::vec3));
        display.Update();
    }
    
    return 0;
}
