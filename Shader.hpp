#ifndef SHADER_HPP
#define SHADER_HPP

#include <string>
#include <fstream>
#include <iostream>
#include <GL/glew.h>

class Shader
{
public:
    Shader(const std::string& shaderName);
    ~Shader();
    void Bind();

private:
    GLuint m_Program;
};

#endif // SHADER_HPP